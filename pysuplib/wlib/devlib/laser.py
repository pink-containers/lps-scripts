from wlib.devlib.basedev import BASEDEV
import epics
import numpy as np
import time

class LASER(BASEDEV):
    def __init__(self, **kwargs):
        self.desc = "Laser device"
    
    def connect(self, **kwargs):
        self.trigburstpv = epics.PV("LPQ:laser:probe:TriggerBurst.PROC", auto_monitor=False)

    def snapshot(self, **kwargs):
        w = kwargs["w"]

        ## pump
        w.save_dataset('instruments/laser/pump/PGConfig', epics.caget("LPQ:laser:pump:PGConf_RBV"))
        w.save_dataset('instruments/laser/pump/DelayStep', epics.caget("LPQ:laser:pump:DelStep_RBV"))
        w.save_dataset('instruments/laser/pump/DelayStep-ps', epics.caget("LPQ:laser:pump:DelStep_ps_RBV"))
        w.save_dataset('instruments/laser/pump/Wavelength', epics.caget("LPQ:laser:pump:Wavelength_RBV"))
        w.save_dataset('instruments/laser/pump/AdjustDelay', epics.caget("LPQ:laser:pump:AdjDelay_RBV"))
        w.save_dataset('instruments/laser/pump/SyncMode', epics.caget("LPQ:laser:pump:SyncMode_RBV"))
        w.save_dataset('instruments/laser/pump/PRR', epics.caget("LPQ:laser:pump:PRR_RBV"))
        w.save_dataset('instruments/laser/pump/EnergyLevel', epics.caget("LPQ:laser:pump:EnergyLevel_RBV"))
        w.save_dataset('instruments/laser/pump/BurstMode', epics.caget("LPQ:laser:pump:BurstMode_RBV"))
        w.save_dataset('instruments/laser/pump/BurstLength', epics.caget("LPQ:laser:pump:BurstLength_RBV"))

        ## probe
        w.save_dataset('instruments/laser/probe/AdjustDelay', epics.caget("LPQ:laser:probe:AdjDelay_RBV"))
        w.save_dataset('instruments/laser/probe/SyncMode', epics.caget("LPQ:laser:probe:SyncMode_RBV"))
        w.save_dataset('instruments/laser/probe/PRR', epics.caget("LPQ:laser:probe:PRR_RBV"))
        w.save_dataset('instruments/laser/probe/AsyncPRR', epics.caget("LPQ:laser:probe:AsyncPRR_RBV"))
        w.save_dataset('instruments/laser/probe/SlaveMode', epics.caget("LPQ:laser:probe:SlaveMode_RBV"))
        w.save_dataset('instruments/laser/probe/EnergyLevel', epics.caget("LPQ:laser:probe:EnergyLevel_RBV"))
        w.save_dataset('instruments/laser/probe/BurstMode', epics.caget("LPQ:laser:probe:BurstMode_RBV"))
        w.save_dataset('instruments/laser/probe/BurstLength', epics.caget("LPQ:laser:probe:BurstLength_RBV"))
        w.save_dataset('instruments/laser/probe/Shutter', epics.caget("LPQ:laser:probe:Shutter_RBV"))

        ## filters
        w.save_dataset('instruments/laser/filters/filter1', epics.caget("LPQ:laseratt:1:fstate"))
        w.save_dataset('instruments/laser/filters/filter2', epics.caget("LPQ:laseratt:2:fstate"))
        w.save_dataset('instruments/laser/filters/filter3', epics.caget("LPQ:laseratt:3:fstate"))
        w.save_dataset('instruments/laser/filters/filter4', epics.caget("LPQ:laseratt:4:fstate"))
        w.save_dataset('instruments/laser/filters/mirror', epics.caget("LPQ:laseratt:5:fstate"))
        w.save_dataset('instruments/laser/filters/filter6', epics.caget("LPQ:laseratt:6:fstate"))

    def burst(self, **kwargs):
        self.trigburstpv.put(1)

  