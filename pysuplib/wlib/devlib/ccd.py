from wlib.devlib.basedev import BASEDEV
import epics
import numpy as np
import time
import threading

class CCD(BASEDEV):
    def __init__(self, **kwargs):
        self.N = str(int(kwargs["N"]))
        self.CCDN = f"ccd{self.N}"
        self.desc = f"{self.CCDN} device"
        self.eve = threading.Event()
        self.eveimg = threading.Event()
        self.evefree = threading.Event()
        self.fulldim = None
        self.refdim = None
        self.samdim = None
        self.srcdim = None

    def connect(self, **kwargs):
        def onchange(**kwargs):
            self.eve.set()
        def onchangeimg(**kwargs):
            self.eveimg.set()
        def onchangefree(**kwargs):
            if kwargs["value"]==0:
                self.evefree.set()
        self.acqpv = epics.PV(f"LPQ:{self.CCDN}:cam1:Acquire", auto_monitor=False)
        self.statepv = epics.PV(f"LPQ:{self.CCDN}:cam1:DetectorState_RBV", auto_monitor=True)
        self.idpv = epics.PV(f"LPQ:{self.CCDN}:cam1:ArrayCounter_RBV", auto_monitor=True, callback=onchange)
        self.fullimagepv = epics.PV(f"LPQ:{self.CCDN}:image1:ArrayData", auto_monitor=True, callback=onchangeimg)
        self.ccdfreepv = epics.PV(f"LPQ:{self.CCDN}:cam1:AcquireBusy", auto_monitor=True, callback=onchangefree)
        #self.refimagepv = epics.PV(f"LPQ:{self.CCDN}:image3:ArrayData", auto_monitor=True)
        #self.srcimagepv = epics.PV(f"LPQ:{self.CCDN}:image4:ArrayData", auto_monitor=True)
        #self.samimagepv = epics.PV(f"LPQ:{self.CCDN}:image5:ArrayData", auto_monitor=True)
        self.cmassxpv = epics.PV(f"LPQ:{self.CCDN}:gfit:X:center_mass", auto_monitor=True)
        self.cmassypv = epics.PV(f"LPQ:{self.CCDN}:gfit:Y:center_mass", auto_monitor=True)
        self.temppv = epics.PV(f"LPQ:{self.CCDN}:cam1:TemperatureActual", auto_monitor=True)
        self.specrefpv = epics.PV(f"LPQ:{self.CCDN}:x:3:specX_RBV", auto_monitor=False)
        self.specsampv = epics.PV(f"LPQ:{self.CCDN}:x:5:specX_RBV", auto_monitor=False)
        self.specabspv = epics.PV(f"LPQ:{self.CCDN}:ABS:data", auto_monitor=False)
        self.resetavgrefpv = epics.PV(f"LPQ:{self.CCDN}:x:3:resetX", auto_monitor=False)
        self.resetavgsampv = epics.PV(f"LPQ:{self.CCDN}:x:5:resetX", auto_monitor=False)
        self.resetavgabspv = epics.PV(f"LPQ:{self.CCDN}:ABS:reset", auto_monitor=False)

    def snapshot(self, **kwargs):
        w = kwargs["w"]
        w.save_dataset('instruments/ccd/xdim', epics.caget(f"LPQ:{self.CCDN}:image1:ArraySize0_RBV"))
        w.save_dataset('instruments/ccd/ydim', epics.caget(f"LPQ:{self.CCDN}:image1:ArraySize1_RBV"))
        w.save_dataset('instruments/ccd/exposure', epics.caget(f"LPQ:{self.CCDN}:cam1:AcquireTime"))
        #w.save_dataset('instruments/ccd/temperature', epics.caget(f"LPQ:{self.CCDN}:cam1:TemperatureActual"))
        w.save_dataset('instruments/ccd/BinningY', epics.caget(f"LPQ:{self.CCDN}:cam1:BinY_RBV"))

        w.save_dataset('instruments/ccd/processed/reference/MinX', epics.caget(f"LPQ:{self.CCDN}:ROI1:MinX"))
        w.save_dataset('instruments/ccd/processed/reference/SizeX', epics.caget(f"LPQ:{self.CCDN}:ROI1:SizeX"))
        w.save_dataset('instruments/ccd/processed/reference/MinY', epics.caget(f"LPQ:{self.CCDN}:ROI1:MinY"))
        w.save_dataset('instruments/ccd/processed/reference/SizeY', epics.caget(f"LPQ:{self.CCDN}:ROI1:SizeY"))
        #w.save_dataset('instruments/ccd/processed/reference/xdim', epics.caget(f"LPQ:{self.CCDN}:image3:ArraySize0_RBV"))
        #w.save_dataset('instruments/ccd/processed/reference/ydim', epics.caget(f"LPQ:{self.CCDN}:image3:ArraySize1_RBV"))

        w.save_dataset('instruments/ccd/processed/sample/MinX', epics.caget(f"LPQ:{self.CCDN}:ROI3:MinX"))
        w.save_dataset('instruments/ccd/processed/sample/SizeX', epics.caget(f"LPQ:{self.CCDN}:ROI3:SizeX"))
        w.save_dataset('instruments/ccd/processed/sample/MinY', epics.caget(f"LPQ:{self.CCDN}:ROI3:MinY"))
        w.save_dataset('instruments/ccd/processed/sample/SizeY', epics.caget(f"LPQ:{self.CCDN}:ROI3:SizeY"))
        #w.save_dataset('instruments/ccd/processed/sample/xdim', epics.caget(f"LPQ:{self.CCDN}:image5:ArraySize0_RBV"))
        #w.save_dataset('instruments/ccd/processed/sample/ydim', epics.caget(f"LPQ:{self.CCDN}:image5:ArraySize1_RBV"))

        w.save_dataset('instruments/ccd/processed/source/MinX', epics.caget(f"LPQ:{self.CCDN}:ROI2:MinX"))
        w.save_dataset('instruments/ccd/processed/source/SizeX', epics.caget(f"LPQ:{self.CCDN}:ROI2:SizeX"))
        w.save_dataset('instruments/ccd/processed/source/MinY', epics.caget(f"LPQ:{self.CCDN}:ROI2:MinY"))
        w.save_dataset('instruments/ccd/processed/source/SizeY', epics.caget(f"LPQ:{self.CCDN}:ROI2:SizeY"))
        #w.save_dataset('instruments/ccd/processed/source/xdim', epics.caget(f"LPQ:{self.CCDN}:image4:ArraySize0_RBV"))
        #w.save_dataset('instruments/ccd/processed/source/ydim', epics.caget(f"LPQ:{self.CCDN}:image4:ArraySize1_RBV"))

    def createdatasets(self, **kwargs):
        w = kwargs["w"]
        # full images
        xdim = epics.caget(f"LPQ:{self.CCDN}:image1:ArraySize0_RBV")
        ydim = epics.caget(f"LPQ:{self.CCDN}:image1:ArraySize1_RBV")
        self.fulldim = (ydim,xdim)
        self.fulldpath = "instruments/ccd/raw/images"
        w.create_dataset(self.fulldpath, dtype=np.uint16, dimensions=self.fulldim, compression=True)
        # reference
        #xdim = epics.caget(f"LPQ:{self.CCDN}:image3:ArraySize0_RBV")
        #ydim = epics.caget(f"LPQ:{self.CCDN}:image3:ArraySize1_RBV")
        #self.refdim = (ydim,xdim)
        #self.refdpath = "instruments/ccd/processed/reference/images"
        #w.create_dataset(self.refdpath, dtype=np.uint16, dimensions=self.refdim, compression=True)
        # source
        #xdim = epics.caget(f"LPQ:{self.CCDN}:image4:ArraySize0_RBV")
        #ydim = epics.caget(f"LPQ:{self.CCDN}:image4:ArraySize1_RBV")
        #self.srcdim = (ydim,xdim)
        #self.srcdpath = "instruments/ccd/processed/source/images"
        #w.create_dataset(self.srcdpath, dtype=np.uint16, dimensions=self.srcdim, compression=True)
        # sample
        #xdim = epics.caget(f"LPQ:{self.CCDN}:image5:ArraySize0_RBV")
        #ydim = epics.caget(f"LPQ:{self.CCDN}:image5:ArraySize1_RBV")
        #self.samdim = (ydim,xdim)
        #self.samdpath = "instruments/ccd/processed/sample/images"
        #w.create_dataset(self.samdpath, dtype=np.uint16, dimensions=self.samdim, compression=True)
        # source center of mass
        self.srccmassxdpath = "instruments/ccd/processed/source/CenterMassX"
        self.srccmassydpath = "instruments/ccd/processed/source/CenterMassY"
        w.create_dataset(self.srccmassxdpath, dtype=np.float64)
        w.create_dataset(self.srccmassydpath, dtype=np.float64)
        self.tempdpath = "instruments/ccd/temperature"
        w.create_dataset(self.tempdpath, dtype=np.float64)
        self.ccdiddpath = "instruments/ccd/raw/ids"
        w.create_dataset(self.ccdiddpath, dtype=np.float64)
        # spec reference
        self.specrefdpath = "instruments/ccd/processed/spectra/reference"
        self.specrefdim = int(epics.caget(f"LPQ:{self.CCDN}:ROI1:SizeX"))
        w.create_dataset(self.specrefdpath, dtype=np.int32, dimensions=self.specrefdim)
        # spec sample
        self.specsamdpath = "instruments/ccd/processed/spectra/sample"
        self.specsamdim = int(epics.caget(f"LPQ:{self.CCDN}:ROI3:SizeX"))
        w.create_dataset(self.specsamdpath, dtype=np.int32, dimensions=self.specsamdim)
        # spec absorption
        self.specabsdpath = "instruments/ccd/processed/spectra/absorption"
        self.specabsdim = min(self.specrefdim, self.specsamdim)
        w.create_dataset(self.specabsdpath, dtype=np.float64, dimensions=self.specabsdim)
        # create hard links
        self.createlinks(w)

    def createlinks(self, w):
        w.save_hardlink("data/images", self.fulldpath)
        w.save_hardlink("data/reference", self.specrefdpath)
        w.save_hardlink("data/sample", self.specsamdpath)
        w.save_hardlink("data/absorption", self.specabsdpath)

    def appenddata(self, **kwargs):
        w = kwargs["w"]
        # images
        w.append_dataset(self.fulldpath, np.reshape(self.tou16(self.fullimagepv.get()), self.fulldim))
        #w.append_dataset(self.refdpath, np.reshape(self.tou16(self.refimagepv.get()), self.refdim))
        #w.append_dataset(self.srcdpath, np.reshape(self.tou16(self.srcimagepv.get()), self.srcdim))
        #w.append_dataset(self.samdpath, np.reshape(self.tou16(self.samimagepv.get()), self.samdim))
        # cmass
        w.append_dataset(self.srccmassxdpath, self.cmassxpv.get())
        w.append_dataset(self.srccmassydpath, self.cmassypv.get())
        # temperature
        w.append_dataset(self.tempdpath, self.temppv.get())
        # ccd ids
        w.append_dataset(self.ccdiddpath, self.idpv.get())
        # spectra
        w.append_dataset(self.specrefdpath, self.specrefpv.get())
        w.append_dataset(self.specsamdpath, self.specsampv.get())
        w.append_dataset(self.specabsdpath, self.specabspv.get())

    def set_exposure(self, exposure):
        epics.caput(f"LPQ:{self.CCDN}:cam1:AcquireTime", exposure)

    def set_autosave(self, astate):
        epics.caput(f"LPQ:{self.CCDN}:TIFF1:AutoSave", astate)

    def get_autosave(self):
        return int(epics.caget(f"LPQ:{self.CCDN}:TIFF1:AutoSave"))       

    def set_mode(self, mode):
        ## mode 0 : single
        ## mode 1 : multiple
        ## mode 2 : continuous
        epics.caput(f"LPQ:{self.CCDN}:cam1:ImageMode", mode)

    def waitccdidle(self, timeout):
        state = self.statepv.get()
        err = False
        t0 = time.time()
        while(state):
            state = self.statepv.get()
            if ((time.time()-t0)>timeout):
                err=True
                state=0
                self.ccderr("Timeout waiting for idle state")
            time.sleep(0.5)
        return err

    def waitnewid(self, timeout):
        self.eve.clear()
        err = not self.eve.wait(timeout)
        self.eve.clear()
        return err

    def waitnewimg(self, timeout):
        self.eveimg.clear()
        err = not self.eveimg.wait(timeout)
        self.eveimg.clear()
        return err

    def waitccdfree(self, timeout):
        self.evefree.clear()
        err = not self.evefree.wait(timeout)
        self.evefree.clear()
        return err

    def ccderr(self, msg):
        self.acqpv.put(0)
        self.set_mode(0)
        outmsg = f"[{self.desc}][{time.asctime()}] {msg}"
        print(outmsg)

    def acquire(self):
        self.acqpv.put(1)

    def tou16(self, inarray):
        return np.array(inarray, dtype=np.uint16)

    def resetaverages(self):
        self.resetavgrefpv.put(1)
        self.resetavgsampv.put(1)
        self.resetavgabspv.put(1)

    def getbackground(self, **kwargs):
        bgparams=kwargs["bgparams"]
        exposure = bgparams["exposure"]
        discard = int(bgparams["discard"])
        images = int(bgparams["images"])
        before = int(bgparams["before"])
        cancelpv = bgparams["cancelpv"]
        statuspv = bgparams["statuspv"]
        progbarpv = bgparams["progbarpv"]
        w = bgparams["w"]
        progtotal = bgparams["progtotal"]
        progcounter = bgparams["progcounter"]
        xdim = int(epics.caget(f"LPQ:{self.CCDN}:image1:ArraySize0_RBV"))
        ydim = int(epics.caget(f"LPQ:{self.CCDN}:image1:ArraySize1_RBV"))

        # vars
        err=False
        if before:
            procstr = '2'
            bgpath = 'before'
        else:
            procstr = '3'
            bgpath = 'after'

        if images<1:
            return err
        if cancelpv.get()>0:
            return err

        # stops ccd and wait
        statuspv.put("BG acquisition. Preparing CCD...")
        self.acqpv.put(0)
        err = self.waitccdidle(30)
        if err: return err

        ntotal = discard+images

        # autosave
        asavestate = int(epics.caget(f"LPQ:{self.CCDN}:TIFF1:AutoSave"))
        epics.caput(f"LPQ:{self.CCDN}:TIFF1:AutoSave", 0)

        # setup ccd
        self.set_exposure(exposure)
        epics.caput(f"LPQ:{self.CCDN}:cam1:NumImages", ntotal)
        self.set_mode(1)
        timeout = exposure+30.0

        # set status
        msg = f"Background: 0/{ntotal} images acquired"
        statuspv.put(msg)
        
        # create dataset
        dpathimages = f"instruments/ccd/background/{bgpath}/images"
        w.create_dataset(dpathimages, dtype=np.uint16, dimensions=(ydim,xdim), compression=True)
        dpathids = f"instruments/ccd/background/{bgpath}/ids"
        w.create_dataset(dpathids, dtype=np.uint32)

        # start acquisition
        self.acqpv.put(1)
        saveframe=False
        for i in range(ntotal):
            if cancelpv.get()>0:
                self.acqpv.put(0)
                return
            err = self.waitnewimg(timeout)
            id = i+1

            #handle timeout error
            if err==True:
                msg = "timeout waiting for new image"
                statuspv.put(msg)
                self.ccderr(msg)
                return True

            # handle discard images
            if (discard>0):
                if (id>discard):
                    saveframe = True
            else:
                saveframe = True

            ## update status
            msg = "Background: {}/{} images acquired".format(id,ntotal)
            statuspv.put(msg)
            bgparams["progcounter"]+=1
            progbarpv.put((100.0*(bgparams["progcounter"]/progtotal)))              
            ## Save valid frames
            if saveframe:
                rawimage = self.tou16(self.fullimagepv.get())
                w.append_dataset(dpathimages, np.reshape(rawimage, (ydim,xdim)))
                w.append_dataset(dpathids, int(self.idpv.get()))

        # set autosave to previous value
        epics.caput(f"LPQ:{self.CCDN}:TIFF1:AutoSave", asavestate)

        # only returns after ccd is idle
        err = self.waitccdidle(30)
        self.set_mode(0)
        return err

        ###  End of getbackground

    def log(self, msg):
        print(f"[{time.time()}] log: {msg}")
