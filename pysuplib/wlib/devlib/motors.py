from wlib.devlib.basedev import BASEDEV
import epics
import numpy as np
import time

class MOTORS(BASEDEV):
    def __init__(self, **kwargs):
        self.desc = "Motors device"

    def snapshot(self, **kwargs):
        w = kwargs["w"]
        ## Branch A
        # ZP ref
        w.save_dataset('instruments/motors/A/zoneplates/reference/X', epics.caget("LPQ:sma1:m1.RBV"))
        w.save_dataset('instruments/motors/A/zoneplates/reference/Gonio', epics.caget("LPQ:sma1:m2.RBV"))
        w.save_dataset('instruments/motors/A/zoneplates/reference/Rot', epics.caget("LPQ:sma1:m3.RBV"))
        w.save_dataset('instruments/motors/A/zoneplates/reference/Y', epics.caget("LPQ:sma1:m4.RBV"))
        # ZP sam
        w.save_dataset('instruments/motors/A/zoneplates/sample/X', epics.caget("LPQ:sma1:m10.RBV"))
        w.save_dataset('instruments/motors/A/zoneplates/sample/Gonio', epics.caget("LPQ:sma1:m11.RBV"))
        w.save_dataset('instruments/motors/A/zoneplates/sample/Rot', epics.caget("LPQ:sma1:m12.RBV"))
        w.save_dataset('instruments/motors/A/zoneplates/sample/Y', epics.caget("LPQ:sma1:m13.RBV"))
        # slits ref
        w.save_dataset('instruments/motors/A/slits/reference/Top', epics.caget("LPQ:sma1:m5.RBV"))
        w.save_dataset('instruments/motors/A/slits/reference/Bottom', epics.caget("LPQ:sma1:m6.RBV"))
        w.save_dataset('instruments/motors/A/slits/reference/Right', epics.caget("LPQ:sma1:m7.RBV"))
        w.save_dataset('instruments/motors/A/slits/reference/Left', epics.caget("LPQ:sma1:m8.RBV"))
        # slits sam
        w.save_dataset('instruments/motors/A/slits/sample/Top', epics.caget("LPQ:sma1:m14.RBV"))
        w.save_dataset('instruments/motors/A/slits/sample/Bottom', epics.caget("LPQ:sma1:m15.RBV"))
        w.save_dataset('instruments/motors/A/slits/sample/Right', epics.caget("LPQ:sma1:m16.RBV"))
        w.save_dataset('instruments/motors/A/slits/sample/Left', epics.caget("LPQ:sma1:m17.RBV"))
        # sample holder
        w.save_dataset('instruments/motors/A/sampleholder/reference', epics.caget("LPQ:sma3:m1.RBV"))
        w.save_dataset('instruments/motors/A/sampleholder/sample', epics.caget("LPQ:sma3:m4.RBV"))
        # filters
        w.save_dataset('instruments/motors/A/filters/reference', epics.caget("LPQ:sma1:m9.RBV"))
        w.save_dataset('instruments/motors/A/filters/sample', epics.caget("LPQ:sma1:m18.RBV"))

        ## Branch B
        # ZP ref
        w.save_dataset('instruments/motors/B/zoneplates/reference/X', epics.caget("LPQ:sma2:m1.RBV"))
        w.save_dataset('instruments/motors/B/zoneplates/reference/Gonio', epics.caget("LPQ:sma2:m2.RBV"))
        w.save_dataset('instruments/motors/B/zoneplates/reference/Rot', epics.caget("LPQ:sma2:m3.RBV"))
        w.save_dataset('instruments/motors/B/zoneplates/reference/Y', epics.caget("LPQ:sma2:m4.RBV"))
        # ZP sam
        w.save_dataset('instruments/motors/B/zoneplates/sample/X', epics.caget("LPQ:sma2:m10.RBV"))
        w.save_dataset('instruments/motors/B/zoneplates/sample/Gonio', epics.caget("LPQ:sma2:m11.RBV"))
        w.save_dataset('instruments/motors/B/zoneplates/sample/Rot', epics.caget("LPQ:sma2:m12.RBV"))
        w.save_dataset('instruments/motors/B/zoneplates/sample/Y', epics.caget("LPQ:sma2:m13.RBV"))
        # slits ref
        w.save_dataset('instruments/motors/B/slits/reference/Top', epics.caget("LPQ:sma2:m5.RBV"))
        w.save_dataset('instruments/motors/B/slits/reference/Bottom', epics.caget("LPQ:sma2:m6.RBV"))
        w.save_dataset('instruments/motors/B/slits/reference/Right', epics.caget("LPQ:sma2:m7.RBV"))
        w.save_dataset('instruments/motors/B/slits/reference/Left', epics.caget("LPQ:sma2:m8.RBV"))
        # slits sam
        w.save_dataset('instruments/motors/B/slits/sample/Top', epics.caget("LPQ:sma2:m14.RBV"))
        w.save_dataset('instruments/motors/B/slits/sample/Bottom', epics.caget("LPQ:sma2:m15.RBV"))
        w.save_dataset('instruments/motors/B/slits/sample/Right', epics.caget("LPQ:sma2:m16.RBV"))
        w.save_dataset('instruments/motors/B/slits/sample/Left', epics.caget("LPQ:sma2:m17.RBV"))
        # sample holder
        #w.save_dataset('instruments/motors/B/sampleholder/reference', epics.caget("LPQ:sma3:m7.RBV"))
        #w.save_dataset('instruments/motors/B/sampleholder/sample', epics.caget("LPQ:sma3:m10.RBV"))
        # filters
        w.save_dataset('instruments/motors/B/filters/reference', epics.caget("LPQ:sma2:m9.RBV"))
        w.save_dataset('instruments/motors/B/filters/sample', epics.caget("LPQ:sma2:m18.RBV"))
