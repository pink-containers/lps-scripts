from wlib.devlib.basedev import BASEDEV
import epics
import numpy as np


class VALVES(BASEDEV):
    def snapshot(self, **kwargs):
        w = kwargs["w"]
        ## A
        w.save_dataset('instruments/valves/A/V1', epics.caget("LPQ:ct1:State_V1"))
        w.save_dataset('instruments/valves/A/V2', epics.caget("LPQ:ct1:State_V2"))
        w.save_dataset('instruments/valves/A/V3', epics.caget("LPQ:ct1:State_V3"))
        ## B
        w.save_dataset('instruments/valves/B/V1', epics.caget("LPQ:ct2:State_V1"))
        w.save_dataset('instruments/valves/B/V2', epics.caget("LPQ:ct2:State_V2"))
        w.save_dataset('instruments/valves/B/V3', epics.caget("LPQ:ct2:State_V3"))
        w.save_dataset('instruments/valves/B/V4', epics.caget("LPQ:ct2:State_V4"))
        ## Sample holder switch
        w.save_dataset('instruments/valves/A/SampleHolder', epics.caget("LPQ:ct1:State_S1"))
        w.save_dataset('instruments/valves/B/SampleHolder', epics.caget("LPQ:ct2:State_S1"))
