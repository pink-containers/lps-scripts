from devlib.basedev import BASEDEV
import epics
import numpy as np

class DEV1(BASEDEV):
    def __init__(self, **kwargs):
        self.desc = "Test device 1"
        self.betapv = None

    def connect(self, **kwargs):
        self.betapv = epics.PV("PC:dev:1:beta", auto_monitor=True)

    def get_alpha(self, **kwargs):
        return epics.caget("PC:dev:1:alpha")

    def get_beta(self, **kwargs):
        return self.betapv.get()
    
    def snapshot(self, **kwargs):
        w = kwargs["w"]
        w.save_dataset('instruments/dev1/alpha', epics.caget("PC:dev:1:alpha"))

    def createdatasets(self, **kwargs):
        w = kwargs["w"]
        w.create_dataset("instruments/dev1/beta", dtype=np.float64)

    def appenddata(self, **kwargs):
        w = kwargs["w"]
        w.append_dataset("instruments/dev1/beta", self.betapv.get())