from wlib.devlib.basedev import BASEDEV
import epics
import numpy as np

class SAMPLE(BASEDEV):
    def snapshot(self, **kwargs):
        w = kwargs["w"]
        # sample info
        w.save_dataset('parameters/sample', epics.caget("LPQ:ccd2:progress_text3", as_string=True))
