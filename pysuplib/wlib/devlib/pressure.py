from wlib.devlib.basedev import BASEDEV
import epics
import numpy as np
import time

class PRESSURE(BASEDEV):
    def __init__(self, **kwargs):
        self.desc = "pressure device"

    def connect(self, **kwargs):
        self.plasmapv = epics.PV("LPQ:tpg1:S1Measure", auto_monitor=True)
        self.asamplepv = epics.PV("LPQ:tpg1:S2Measure", auto_monitor=True)
        self.aopticspv = epics.PV("LPQ:tpg1:S3Measure", auto_monitor=True)
        self.adetectorpv = epics.PV("LPQ:tpg1:S5Measure", auto_monitor=True)
        self.plasmacontrolpv = epics.PV("LPQ:tpg2:S1Measure", auto_monitor=True)
        self.bsamplepv = epics.PV("LPQ:tpg2:S2Measure", auto_monitor=True)
        self.bopticspv = epics.PV("LPQ:tpg2:S3Measure", auto_monitor=True)
        self.bcoolingpv = epics.PV("LPQ:tpg2:S4Measure", auto_monitor=True)
        self.bdetectorpv = epics.PV("LPQ:tpg2:S5Measure", auto_monitor=True)

    def createdatasets(self, **kwargs):
        w = kwargs["w"]
        w.create_dataset("instruments/pressure/Plasma", dtype=np.float64)
        w.create_dataset("instruments/pressure/PlasmaControl", dtype=np.float64)
        w.create_dataset("instruments/pressure/A/Sample", dtype=np.float64)
        w.create_dataset("instruments/pressure/A/Optics", dtype=np.float64)
        w.create_dataset("instruments/pressure/A/Detector", dtype=np.float64)
        w.create_dataset("instruments/pressure/B/Sample", dtype=np.float64)
        w.create_dataset("instruments/pressure/B/Optics", dtype=np.float64)
        w.create_dataset("instruments/pressure/B/Cooling", dtype=np.float64)
        w.create_dataset("instruments/pressure/B/Detector", dtype=np.float64)

    def appenddata(self, **kwargs):
        w = kwargs["w"]
        w.append_dataset("instruments/pressure/Plasma", self.plasmapv.get())
        w.append_dataset("instruments/pressure/PlasmaControl", self.plasmacontrolpv.get())
        w.append_dataset("instruments/pressure/A/Sample", self.asamplepv.get())
        w.append_dataset("instruments/pressure/A/Optics", self.aopticspv.get())
        w.append_dataset("instruments/pressure/A/Detector", self.adetectorpv.get())
        w.append_dataset("instruments/pressure/B/Sample", self.bsamplepv.get())
        w.append_dataset("instruments/pressure/B/Optics", self.bopticspv.get())
        w.append_dataset("instruments/pressure/B/Cooling", self.bcoolingpv.get())
        w.append_dataset("instruments/pressure/B/Detector", self.bdetectorpv.get())


