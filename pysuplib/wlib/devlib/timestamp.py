from wlib.devlib.basedev import BASEDEV
import epics
import time
import numpy as np

class TSDEV(BASEDEV):
    def __init__(self, **kwargs):
        self.desc = "Timestamp device"

    def tsnow(self):
        return np.int64(round(time.time()*1000))

    def createdatasets(self, **kwargs):
        w = kwargs["w"]
        w.create_dataset("instruments/timestamps/ts", dtype=np.int64)

    def appenddata(self, **kwargs):
        w = kwargs["w"]
        w.append_dataset("instruments/timestamps/ts", self.tsnow())