from wlib.devlib.basedev import BASEDEV
import epics
import numpy as np
import time

class SOURCE(BASEDEV):
    def __init__(self, **kwargs):
        self.desc = "source device"

    def connect(self, **kwargs):
        self.TRpv = epics.PV("LPQ:source:axis:1:pos_units_RBV", auto_monitor=True)
        self.TTpv = epics.PV("LPQ:source:axis:2:pos_units_RBV", auto_monitor=True)
        self.TCpv = epics.PV("LPQ:source:axis:3:pos_units_RBV", auto_monitor=True)
        self.FLZpv = epics.PV("LPQ:source:axis:4:pos_units_RBV", auto_monitor=True)
        self.MAPpv = epics.PV("LPQ:source:axis:5:pos_units_RBV", auto_monitor=True)
        self.MARpv = epics.PV("LPQ:source:axis:6:pos_units_RBV", auto_monitor=True)
        self.MBPpv = epics.PV("LPQ:source:axis:7:pos_units_RBV", auto_monitor=True)
        self.MBRpv = epics.PV("LPQ:source:axis:8:pos_units_RBV", auto_monitor=True)
        self.q1pv = epics.PV("LPQ:source:quad1", auto_monitor=True)
        self.q2pv = epics.PV("LPQ:source:quad2", auto_monitor=True)
        self.q3pv = epics.PV("LPQ:source:quad3", auto_monitor=True)
        self.q4pv = epics.PV("LPQ:source:quad4", auto_monitor=True)
        self.xpv = epics.PV("LPQ:source:quad5", auto_monitor=True)
        self.ypv = epics.PV("LPQ:source:quad6", auto_monitor=True)
        self.singleperiodpv = epics.PV("LPQ:source:singleperiod", auto_monitor=False)
        self.singletriggerpv = epics.PV("LPQ:source:singletrigger.PROC", auto_monitor=False)
    
    def createdatasets(self, **kwargs):
        w = kwargs["w"]
        w.create_dataset("instruments/source/motors/TR", dtype=np.float64)
        w.create_dataset("instruments/source/motors/TT", dtype=np.float64)
        w.create_dataset("instruments/source/motors/TC", dtype=np.float64)
        w.create_dataset("instruments/source/motors/FLZ", dtype=np.float64)
        w.create_dataset("instruments/source/motors/MaP", dtype=np.float64)
        w.create_dataset("instruments/source/motors/MaR", dtype=np.float64)
        w.create_dataset("instruments/source/motors/MbP", dtype=np.float64)
        w.create_dataset("instruments/source/motors/MbR", dtype=np.float64)
        w.create_dataset("instruments/source/diodes/Q1", dtype=np.float64)
        w.create_dataset("instruments/source/diodes/Q2", dtype=np.float64)
        w.create_dataset("instruments/source/diodes/Q3", dtype=np.float64)
        w.create_dataset("instruments/source/diodes/Q4", dtype=np.float64)
        w.create_dataset("instruments/source/diodes/X", dtype=np.float64)
        w.create_dataset("instruments/source/diodes/Y", dtype=np.float64)
    
    def appenddata(self, **kwargs):
        w = kwargs["w"]    
        w.append_dataset("instruments/source/motors/TR", self.TRpv.get())
        w.append_dataset("instruments/source/motors/TT", self.TTpv.get())
        w.append_dataset("instruments/source/motors/TC", self.TCpv.get())
        w.append_dataset("instruments/source/motors/FLZ", self.FLZpv.get())
        w.append_dataset("instruments/source/motors/MaP", self.MAPpv.get())
        w.append_dataset("instruments/source/motors/MaR", self.MARpv.get())
        w.append_dataset("instruments/source/motors/MbP", self.MBPpv.get())
        w.append_dataset("instruments/source/motors/MbR", self.MBRpv.get())
        w.append_dataset("instruments/source/diodes/Q1", self.q1pv.get())
        w.append_dataset("instruments/source/diodes/Q2", self.q2pv.get())
        w.append_dataset("instruments/source/diodes/Q3", self.q3pv.get())
        w.append_dataset("instruments/source/diodes/Q4", self.q4pv.get())
        w.append_dataset("instruments/source/diodes/X", self.xpv.get())
        w.append_dataset("instruments/source/diodes/Y", self.ypv.get())

    def setsingleperiod(self, period):
        self.singleperiodpv.put(float(period))

    def triggerprocess(self, **kwargs):
        self.singletriggerpv.put(1)