from devlib.basedev import BASEDEV
import epics
import numpy as np

class DEV2(BASEDEV):
    def __init__(self, **kwargs):
        self.desc = "Test device 2"
        self.betapv = None

    def connect(self, **kwargs):
        self.betapv = epics.PV("PC:dev:2:beta", auto_monitor=True)

    def snapshot(self, **kwargs):
        w = kwargs["w"]
        w.save_dataset('instruments/dev2/alpha', epics.caget("PC:dev:2:alpha"))

    def createdatasets(self, **kwargs):
        w = kwargs["w"]
        w.create_dataset("instruments/dev2/beta", dtype=np.float64)

    def appenddata(self, **kwargs):
        w = kwargs["w"]
        w.append_dataset("instruments/dev2/beta", self.betapv.get())