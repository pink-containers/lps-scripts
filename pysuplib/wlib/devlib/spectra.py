from devlib.basedev import BASEDEV
import epics
import numpy as np
import time

class SPECTRA(BASEDEV):
    def __init__(self, **kwargs):
        self.N = str(int(kwargs["N"]))
        self.CCDN = f"ccd{N}"
        self.desc = f"Spectra for {self.CCDN} device"
    
    def connect(self, **kwargs):
        self.refspecpv = epics.PV(f"LPQ:{self.CCDN}:x:3:specX_RBV", auto_monitor=True)
        self.samspecpv = epics.PV(f"LPQ:{self.CCDN}:x:5:specX_RBV", auto_monitor=True)
        self.absspecpv = epics.PV(f"LPQ:{self.CCDN}:ABS:data", auto_monitor=True)

    def createdatasets(self, **kwargs):
        w = kwargs["w"]
        self.refsize = int(epics.caget("LPQ:ccd2:ROI1:SizeX_RBV"))
        self.samsize = int(epics.caget("LPQ:ccd2:ROI3:SizeX_RBV"))
        self.abssize = min(self.refsize, self.samsize)

        w.create_dataset("data/spectra/reference", dtype=np.float64, dimensions=self.refsize)
        w.create_dataset("data/spectra/sample", dtype=np.float64, dimensions=self.samsize)
        w.create_dataset("data/spectra/absorption", dtype=np.float64, dimensions=self.abssize)

    def appenddata(self, **kwargs):
        w = kwargs["w"]
        w.append_dataset("data/spectra/reference", self.refspecpv.get())
        w.append_dataset("data/spectra/sample", self.samspecpv.get())
        w.append_dataset("data/spectra/absorption", self.absspecpv.get())
