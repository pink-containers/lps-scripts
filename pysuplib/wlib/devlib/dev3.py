from devlib.basedev import BASEDEV
import epics
import numpy as np
import time
import threading

class DEV3(BASEDEV):
    def __init__(self, **kwargs):
        self.desc = "Test device 3"
        self.betapv = None
        self.eve = threading.Event()

    def connect(self, **kwargs):
        def onchange(**kwargs):
            self.eve.set()
        self.betapv = epics.PV("PC:dev:3:beta", auto_monitor=True, callback=onchange)

    def snapshot(self, **kwargs):
        w = kwargs["w"]
        w.save_dataset('instruments/dev3/alpha', epics.caget("PC:dev:3:alpha"))

    def createdatasets(self, **kwargs):
        w = kwargs["w"]
        w.create_dataset("instruments/dev3/beta", dtype=np.float64)

    def appenddata(self, **kwargs):
        w = kwargs["w"]
        w.append_dataset("instruments/dev3/beta", self.betapv.get())

    def waitnewbeta(self, **kwargs):
        ## timeout in seconds
        if ("timeout" not in kwargs):
            timeout = 1.0
        else:
            timeout = kwargs["timeout"]
        self.eve.clear()
        return self.eve.wait(timeout)
