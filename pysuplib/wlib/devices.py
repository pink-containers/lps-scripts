from wlib.devlib.writer import Writer
from wlib.devlib.ccd import CCD
from wlib.devlib.motors import MOTORS
from wlib.devlib.timestamp import TSDEV
from wlib.devlib.source import SOURCE
from wlib.devlib.laser import LASER
from wlib.devlib.pressure import PRESSURE
from wlib.devlib.valves import VALVES
from wlib.devlib.sample import SAMPLE

def connect(devlist):
    for d in devlist:
        d.connect()

def createdatasets(devlist, w):
    for d in devlist:
        d.createdatasets(w=w)

def appenddata(devlist, w):
    for d in devlist:
        d.appenddata(w=w)

def snapshot(devlist, w):
    for d in devlist:
        d.snapshot(w=w)
