import epics
import threading
import time
import wlib.devices as devices

def run():
    ## data
    datapath = "/EPICS/data"
    datafilename = "branchA"

    ## event
    eve = threading.Event()
    def onchange(**kwargs):
        if kwargs["value"]>0:
            eve.set()

    print("***  fwriter A  ***")

    ## connect to epics hmi PVs
    print(f"[{time.asctime()}] Connecting epics hmi channels")
    runbuttonpv = epics.PV("LPQ:pysup:aux:B02", auto_monitor=True, callback=onchange)
    rebootbuttonpv = epics.PV("LPQ:pysup:aux:B04", auto_monitor=True, callback=onchange)
    cancelbuttonpv = epics.PV("LPQ:pysup:aux:B03", auto_monitor=True)
    statuspv = epics.PV("LPQ:ccd1:progress_text1", auto_monitor=False)
    progbarpv = epics.PV("LPQ:ccd1:progress_value", auto_monitor=False)
    h5filepv = epics.PV("LPQ:ccd1:progress_text2", auto_monitor=False)
    trstatuspv = epics.PV("LPQ:TRBAND:safe", auto_monitor=True)
    trnextpv = epics.PV("LPQ:TRBAND:next", auto_monitor=True)
    time.sleep(2)

    ## create writer
    w = devices.Writer()
    w.set_datapath(datapath)

    def reset():
        eve.clear()
        w.close()
        cancelbuttonpv.put(0)

    ## create devices
    devicelist = []
    ccd = devices.CCD(N=1)
    devicelist.append(ccd)
    motors = devices.MOTORS()
    devicelist.append(motors)
    tsdev = devices.TSDEV()
    devicelist.append(tsdev)
    source = devices.SOURCE()
    devicelist.append(source)
    laser = devices.LASER()
    devicelist.append(laser)
    pressure = devices.PRESSURE()
    devicelist.append(pressure)
    valves = devices.VALVES()
    devicelist.append(valves)
    sample = devices.SAMPLE()
    devicelist.append(sample)

    ## connect devices
    print(f"[{time.asctime()}] Connecting epics devices")
    devices.connect(devicelist)
    time.sleep(4)

    ## main loop
    print(f"[{time.asctime()}] Listening...")
    eve.clear()
    while True:
        try:
            eve.wait()
            ## end function if reboot is pressed
            if rebootbuttonpv.get()>0:
                print(f"[{time.asctime()}] Rebooting")
                return
            eve.clear()

            ## update status
            statuspv.put("Acquisition started")
            progbarpv.put(0)

            ## main code
            ## create new file
            datafilename = epics.caget("LPQ:ccd1:TIFF1:FileName", as_string=True)
            w.newfile(datafilename)
            h5filepv.put(w.filename)

            ## create datasets
            devices.createdatasets(devicelist, w)

            ## capture parameters
            ## input values for branch A
            discard = int(epics.caget("LPQ:pysup:aux:A01"))
            before = int(epics.caget("LPQ:pysup:aux:A02"))
            images = int(epics.caget("LPQ:pysup:aux:A03"))
            shots = int(epics.caget("LPQ:laser:probe:BurstLength_RBV"))
            after = int(epics.caget("LPQ:pysup:aux:A04"))

            ## save parameters
            w.save_dataset("parameters/BGDiscard", discard)
            w.save_dataset("parameters/BGBefore", before)
            w.save_dataset("parameters/Images", images)
            w.save_dataset("parameters/ShotsPerImage", shots)
            w.save_dataset("parameters/BGafter", after)

            ## save start time
            w.save_dataset("parameters/timeStart", time.asctime())

            #progcounter
            progtotal = images+before+after
            if before>0:
                progtotal+=discard
            if after>0:
                progtotal+=discard
            progcounter = 0

            ## calculate timings
            proc_time = (shots/100.0)
            ccdtime = proc_time+1.0
            srctime = proc_time+0.7

            if before>0:
                bgparams = {
                    "exposure" : ccdtime,
                    "discard" : discard,
                    "images" : before,
                    "before" : True,
                    "cancelpv" : cancelbuttonpv,
                    "statuspv" : statuspv,
                    "progbarpv" : progbarpv,
                    "w" : w,
                    "progtotal" : progtotal,
                    "progcounter" : progcounter
                }
                err = ccd.getbackground(bgparams=bgparams)
                progcounter = bgparams["progcounter"]

            ## setup ccd
            err = ccd.waitccdidle(30)
            ## handle cancel button
            if cancelbuttonpv.get()>0:
                statuspv.put("Acquisition cancelled")
                reset()
                continue
            if err:
                statuspv.put("Error waiting for new ccd to be idle. Aborting.")
                reset()
                continue
            ccd.set_exposure(ccdtime)
            ccd.set_mode(0)

            # autosave
            asavestate = ccd.get_autosave()
            #ccd.set_autosave(0)

            # reset averages
            ccd.resetaverages()

            ## setup source
            source.setsingleperiod(srctime)

            statuspv.put("Acquiring: {}/{}, {:.1f}%".format(0,images,0.0))
            for i in range(images):
                # check cancel button
                if cancelbuttonpv.get()>0:
                    statuspv.put("Acquisition cancelled")
                    reset()
                    break
                ## check for available target
                notsafe = int(trstatuspv.value)
                if notsafe:
                    statuspv.put("Moving TR...")
                    t0=time.time()
                    trnextpv.put(1)
                    time.sleep(0.5)
                    isbusy = True
                    while isbusy:
                        isbusy=int(trnextpv.value)
                        dt = time.time()-t0
                        if dt>30.0:
                            statuspv.put("Timeout 30 sec. aborting.")
                            cancelbuttonpv.put(1)
                            isbusy=False
                            time.sleep(3)
                        time.sleep(1)
                    statuspv.put("Continue.")
                # check cancel button
                if cancelbuttonpv.get()>0:
                    statuspv.put("Acquisition cancelled")
                    reset()
                    break
                # main sequence
                source.triggerprocess()
                ccd.acquire()
                time.sleep(0.5)
                #laser.burst()
                # wait for new image
                err = ccd.waitccdfree(ccdtime+10.0)
                if err:
                    statuspv.put("Error waiting for new ccd image")
                    cancelbuttonpv.put(1)
                    break
                # add data to file
                devices.appenddata(devicelist, w)
                # update status
                prog = (100.0*(i+1)/images)
                statuspv.put("Acquiring {}/{}, {:.1f}%".format(i+1,images,prog))
                progcounter+=1
                progbarpv.put((100.0*(progcounter/progtotal)))

            # check cancel button
            if cancelbuttonpv.get()>0:
                statuspv.put("Acquisition cancelled")
                reset()
                continue

            if after>0:
                bgparams = {
                    "exposure" : ccdtime,
                    "discard" : discard,
                    "images" : after,
                    "before" : False,
                    "cancelpv" : cancelbuttonpv,
                    "statuspv" : statuspv,
                    "progbarpv" : progbarpv,
                    "w" : w,
                    "progtotal" : progtotal,
                    "progcounter" : progcounter
                }
                err = ccd.getbackground(bgparams=bgparams)
                ## treat error 

            # check cancel button
            if cancelbuttonpv.get()>0:
                statuspv.put("Acquisition cancelled")
                reset()
                continue

            ## take devices snapshot
            devices.snapshot(devicelist, w)

            ## save end time
            w.save_dataset("parameters/timeEnd", time.asctime())

            ## close file
            w.close()

            # set autosave to previous value
            ccd.set_autosave(asavestate)

            statuspv.put("Acquisition done")

            reset()
            ## end of main loop
        except Exception as errr:
            print(f"[time.asctime()] Exception caught.")
            print(errr)
            statuspv.put("Exception error. Check log")
            reset()

    w.close()
    reset()
