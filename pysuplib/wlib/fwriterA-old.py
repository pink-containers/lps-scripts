import epics
import threading
import time

def run():
    eve = threading.Event()
    def onchange(**kwargs):
        eve.set()

    ## connect to epics buttons
    runbuttonpv = epics.PV("LPQ:pysup:aux:B01", auto_monitor=True, callback=onchange)
    rebootbuttonpv = epics.PV("LPQ:pysup:aux:B02", auto_monitor=True, callback=onchange)
    cancelbuttonpv = epics.PV("LPQ:pysup:aux:B05", auto_monitor=True)
    time.sleep(2)

    def reset():
        eve.clear()
        cancelbuttonpv.put(0)

    ## main loop
    eve.clear()
    while True:
        ## end function if reboot is pressed
        eve.wait()
        if rebootbuttonpv.get()>0:
            return
        eve.clear()

        ## main code
        for i in range(20):
            if cancelbuttonpv.value: break
            print(f"id:{i} : run B")
            time.sleep(1)

        
        reset()
        ## end of main loop
