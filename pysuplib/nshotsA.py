#!/usr/bin/python3
from wlib.fwriterA import run 
import time

## Entrypoint for experiment routine for LPQ branch A
print(f"[{time.asctime()}] fwriterA started")
run()
print(f"[{time.asctime()}] fwriterA ended")
