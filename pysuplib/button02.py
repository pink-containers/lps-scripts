from epics import caget, caput, PV
#import epics
import time
import numpy

def run(**kwargs):
    DEBUG=False
    log = kwargs["log"]
    ## code
    process_start = "LPQ:source:proc_start"
    process_stop = "LPQ:source:proc_stop"
    
    ccd_exposure = "LPQ:ccd1:cam1:AcquireTime"
    ccd_start = "LPQ:ccd1:cam1:Acquire"
    trig_burst = "LPQ:laser:probe:TriggerBurst.PROC"
    
    ccdmode = "LPQ:ccd1:cam1:ImageMode"
    bandsafe = "LPQ:TRBAND:safe"
    nextbut = "LPQ:TRBAND:next"
    ccd01_exposure_RBV = "LPQ:ccd1:cam1:AcquireTime_RBV"

    burst_mode = "LPQ:laser:probe:BurstMode_RBV"
    nshots = "LPQ:laser:probe:BurstLength_RBV"
    ProbeFdevPV = "LPQ:laser:probe:PRR_RBV"
    #ccd02_exposure_RBV = "LPQ:ccd2:cam1:AcquireTime_RBV"
    log("Button 02: start...")
    #t0=time.time()

    PVburstMode = PV(burst_mode)
    burst_mode_val=PVburstMode.get()
    if DEBUG: print("probe Burst Mode state:" + str(burst_mode_val))

    #calculate timings
    N = caget(nshots)
    ProbeFdev = caget(ProbeFdevPV)
    if DEBUG: print("Probe Freq dev is " + str(ProbeFdev))

    proc_time = (N/(100/ProbeFdev))

    if burst_mode_val == 1:
        ccdtime = proc_time+1
        caput(ccd_exposure, ccdtime)
    else:
        ccdtime = proc_time
        caput(ccd_exposure, ccdtime)
    
    caput(ccdmode, 0)

    notsafe = int(caget(bandsafe))

    if notsafe:
        t0=time.time()
        caput(nextbut, 1)
        time.sleep(0.5)
        isbusy = True
        log("[B02] waiting for next button to be done")
        while isbusy:
            isbusy=int(caget(nextbut))
            dt = time.time()-t0
            if dt>30.0:
                log("[B02] waited more than 30 sec. aborting.")
                return
            time.sleep(1)
        log("[B02] is safe")


    #ccd01_exposure_RBV_val=caget(ccd01_exposure_RBV)

    caput(process_start, 1)
    caput(ccd_start, 1)
    if burst_mode_val == 1:
        time.sleep(0.5)
        caput(trig_burst, 1)
        time.sleep(proc_time+0.2)
    else:
        time.sleep(ccdtime+0.2)
    caput(process_stop, 1)

    #t1=time.time()
    #log("Button 02: end. dt: {:.3f}s".format(t1-t0))
    log("Button 02: end")
    ## EOF


