from writer import Writer
import numpy as np
import time

w = Writer()
w.set_datapath("/home/nil/LPQ/lps-scripts/pysuplib/tests")
w.newfile("mytest")
print(w.fullpath)
print(w.filename)
w.save_dataset('/t1/text1', "blue dog")
m1 = np.random.rand(5,10)
w.save_dataset('/t1/m1', m1)

#create random data
print("creating data...")
rdata = []
rdata2 = []
t0=time.time()
for i in range(100):
    rdata.append(np.random.rand(50,100))
for i in range(100):
    rdata2.append(np.random.rand(60))    
t1=time.time()
print("data created, dt: {:.3f} ms".format(1000*(t1-t0)))

time.sleep(1)

# save data
print("saving data...")
dpath = '/t2/rdata'
w.create_dataset(dpath, dtype=np.float64, dimensions=(50,100), compression=True)
id=0
for rd in rdata:
    t0=time.time()
    w.append_dataset(dpath, rd)
    t1=time.time()
    id+=1
    print("id:{}, dt: {:.3f} ms".format(id, 1000*(t1-t0)))

dpath = '/t2/rdata2'
w.create_dataset(dpath, dtype=np.float64, dimensions=60, compression=False)
id=0
for rd in rdata2:
    t0=time.time()
    w.append_dataset(dpath, rd)
    t1=time.time()
    id+=1
    print("id:{}, dt: {:.3f} ms".format(id, 1000*(t1-t0)))


# array test
print("array test...")
dpath = '/t2/counter'
w.create_dataset(dpath, dtype=np.float64)
for i in range(60):
    w.append_dataset(dpath, float(i))
    print(f"id: {i}")
    time.sleep(0.1)
print("array test OK")

## close file
w.close()
del w
print("OK")
