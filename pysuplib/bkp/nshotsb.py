import time
import epics
import numpy as np
from pysuplib.writer import Writer
import threading


class NSHOTSB:
    def __init__(self):
        pass

    def run(self):
        print("Running nshots B...")
        ## event handler
        eve = threading.Event()
        ## epics callback
        def onchange(**kwargs):
            if kwargs["value"]>0:
                eve.set()
        ## epics channels
        print("Connecting epics channels...")
        actionpv = epics.PV("LPQ:pysup:aux:B10", callback=onchange)
        statuspv = epics.PV("LPQ:ccd2:progress_text1", auto_monitor=False)
        progresspv = epics.PV("LPQ:ccd2:progress_value", auto_monitor=False)
        srcStartpv = epics.PV("LPQ:source:proc_start", auto_monitor=False)
        srcStoppv = epics.PV("LPQ:source:proc_stop", auto_monitor=False)
        ccdAcqpv = epics.PV("LPQ:ccd2:cam1:Acquire", auto_monitor=False)
        ccdStatepv = epics.PV("LPQ:ccd2:cam1:DetectorState_RBV", auto_monitor=True)
        cancelpv = epics.PV("LPQ:pysup:aux:B01", auto_monitor=True)
        trigBurstpv = epics.PV("LPQ:laser:probe:TriggerBurst.PROC", auto_monitor=False)
        time.sleep(2)

        ## main
        eve.clear()
        print("App ready. Listening...")
        eve.wait()
        print("Button pressed!")

        ##start writer
        w = Writer()
        w.set_datapath("/EPICS/test")
        tempfname = epics.caget("LPQ:ccd2:TIFF1:FileName",as_string=True)
        w.newfile(epics.caget("LPQ:ccd2:TIFF1:FileName",as_string=True))

        ## input values
        bgDiscard = int(epics.caget("LPQ:pysup:aux:A05"))
        bgBefore = int(epics.caget("LPQ:pysup:aux:A06"))
        nImages = int(epics.caget("LPQ:pysup:aux:A07"))
        nShots = int(epics.caget("LPQ:laser:probe:BurstLength_RBV"))
        bgAfter = int(epics.caget("LPQ:pysup:aux:A08"))

        ## Save initial data
        w.save_dataset('/Background/params/discard', bgDiscard)
        w.save_dataset('/Background/params/before', bgBefore)
        w.save_dataset('/Background/params/images', nImages)
        w.save_dataset('/Background/params/after', bgAfter)

        ## Start process
        progresspv.put(0)

        ## calculate timings
        proc_time = (nShots/100.0)
        ccdtime = proc_time+1.0


        ## close file
        w.close()
        del w
        

