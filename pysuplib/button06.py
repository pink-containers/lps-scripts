import epics
import time
import numpy

def run(**kwargs):
    log = kwargs["log"]
    ## code
    log("Button 06 - Set laser to burst mode")
    ## PV names
    SourceStart = "LPQ:source:proc_start"
    SourceStop = "LPQ:source:proc_stop"
    ProbeEnergyLevel = "LPQ:laser:probe:EnergyLevel"
    ProbeBurstMode = "LPQ:laser:probe:BurstMode"
    ## sequence

    ## EOF
