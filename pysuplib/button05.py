from epics import caput,caget
import time
import numpy

def run(**kwargs):
    log = kwargs["log"]
    ## code
    log("Button 05")
    ## var
    start=-15000
    step=1000
    N=31
    sam_offset=0
    filename = "calibration_740_A"
    ## code
    posref = []
    for i in range(N):
      posref.append(i*step+start)
    log(f"posref={posref}")

    ##PVs
    refpv = "LPQ:off:master_A_ref"
    sampv = "LPQ:off:master_A_sam"
    fnamepv = "LPQ:ccd1:TIFF1:FileName"
    magic = "LPQ:pysup:B02"
    ccdid = "LPQ:ccd1:cam1:ArrayCounter_RBV"
    status = "LPQ:ccd1:progress_text1"
    cancel = "LPQ:pysup:aux:B01"
    pbar = "LPQ:ccd1:progress_value"
    ##loop
    counter=0
    caput(pbar, 0)
    for ref in posref:
      sam = ref+sam_offset
      fname =f"{filename}_{int(ref)}_{int(sam)}"
      caput(refpv, ref)
      caput(sampv, sam)
      caput(fnamepv, fname)
      idlast = int(caget(ccdid))
      ##wait motors
      time.sleep(3)
      caput(magic, 1)
      time.sleep(1)
      busy=True
      while busy:
        idnow = int(caget(ccdid))
        if idnow!=idlast:
          busy=False
        if caget(cancel)>0:
          return
        time.sleep(0.5)
      time.sleep(1)
      counter+=1
      caput(status, f"Image: {counter}/{N}")
      caput(pbar, 100*counter/N)
    ## EOF
