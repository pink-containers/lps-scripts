import epics
import time
import numpy

def run(**kwargs):
    log = kwargs["log"]
    ## code
    log("Button 08: start")
    proberun = "LPQ:laser:probe:Run.PROC"
    probestop = "LPQ:laser:probe:Stop.PROC"
    syncmode = "LPQ:laser:probe:SyncMode" #int=0 : ext=1

    log("Button 08: ok")
    ## EOF
