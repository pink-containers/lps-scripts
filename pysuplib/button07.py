import epics
import time
import numpy

def run(**kwargs):
    log = kwargs["log"]
    ## code
    log("Button 07 - Multiple shots - Side B")
    epics.caput("LPQ:ccd2:progress_text1", "Acquisition started...")
    epics.caput("LPQ:ccd2:progress_value", 0)

    def checkcancel(cancelpv):
        cb = int(cancelpv.get())
        resp = False
        if cb:
            epics.caput("LPQ:ccd2:progress_text1", "Cancelled")
            epics.caput("LPQ:ccd2:cam1:Acquire", 0)
            epics.caput("LPQ:ccd2:cam1:ImageMode", 0)
            cancelpv.put(0)
            resp=True
        return resp

    def ccdwait(ccdStatepv, cancelpv):
        busy=True
        resp=0
        while busy:
            if int(ccdStatepv.get())==0:
                busy=False
                resp=0
            if checkcancel(cancelpv):
                busy=False
                resp=1
            time.sleep(0.1)
        return resp

    def waitfornewid(idpv, timeout):
        current = int(epics.caget(idpv))
        t0 = time.time()
        new=current
        while(new==current):
            time.sleep(0.5)
            new = int(epics.caget(idpv))
            if ((time.time()-t0)>timeout):
                log("CCD timeout")
                return
    def waitccdidle(statepv, timeout):
        state = 1
        t0 = time.time()
        while(state):
            state = int(epics.caget(statepv))
            if ((time.time()-t0)>timeout):
                state=0
            time.sleep(0.5)

    def cleanexit(chidlist):
        epics.caput("LPQ:ccd2:cam1:Acquire", 0)
        n=0
        for chid in chidlist:
            try:
                epics.ca.clear_channel(chid)
            except Exception as err:
                log("Could not clear epics channel {} on chidlist".format(n))
                log(err)
            n+=1

    ## Background function
    def ccdbg(ccdtime=1, bgDiscard=0, bgN=0, proc=2, statuspv=None, cancelpv=None):
        if bgN<1:
            return
        statuspv.put("BG acquisition. Preparing CCD...")

        epics.caput("LPQ:ccd2:cam1:GreatEyesSync", 0) #disabling sync out
                
        waitccdidle("LPQ:ccd2:cam1:DetectorState_RBV", 30)
        #setup filter
        procstr = "{:d}".format(proc)
        ntotal = bgDiscard+bgN
        progbarpv = "LPQ:ccd2:progress_value"
        imgIDpv = "LPQ:ccd2:cam1:ArrayCounter_RBV"
        #imgID = epics.PV("LPQ:ccd2:cam1:ArrayCounter_RBV", auto_monitor=True)
        nextid = int(epics.caget("LPQ:ccd2:TIFF1:FileNumber_RBV"))
        #log(nextid)
        asavestate = int(epics.caget("LPQ:ccd2:TIFF1:AutoSave"))
        epics.caput("LPQ:ccd2:TIFF1:AutoSave", 0)
        epics.caput(f"LPQ:ccd2:Proc{procstr}:NumFilter", bgN)
        epics.caput(f"LPQ:ccd2:Proc{procstr}:ResetFilter", 1)
        #setup ccd
        epics.caput("LPQ:ccd2:cam1:AcquireTime", ccdtime)
        epics.caput("LPQ:ccd2:cam1:NumImages", ntotal)
        epics.caput("LPQ:ccd2:cam1:ImageMode", 1)
        idstart = int(epics.caget(imgIDpv))
        msg = "BG: 0/0 images acquired"
        statuspv.put(msg)
        epics.caput(progbarpv, 0)
        timeout = ccdtime+30.0
        bgDiscard = int(bgDiscard)
        epics.caput("LPQ:ccd2:cam1:Acquire",1, wait=False)
        ## image acquisition loop
        for i in range(ntotal):
            id=i+1
            waitfornewid(imgIDpv, timeout)
            ##check cancel button
            if int(cancelpv.get()):
                epics.caput("LPQ:ccd2:cam1:Acquire",0)
                time.sleep(1)
                epics.caput("LPQ:ccd2:cam1:ImageMode", 0)
                return
            ## reset filter for discarded images
            if (bgDiscard>0) and (id==bgDiscard):
                time.sleep(0.2)
                epics.caput(f"LPQ:ccd2:Proc{procstr}:ResetFilter", 1)
            ## update status
            msg = "BG: {}/{} images acquired".format(id,ntotal)
            statuspv.put(msg)
            epics.caput(progbarpv, (100.0*id/ntotal))

        ## done with acquisition
        epics.caput(progbarpv, 100.0)

        #save file
        if proc==3:
            fext = "after"
            nextid -= 1
        else:
            fext = "before"

        epics.caput(f"LPQ:ccd2:TIFF{procstr}:FilePath",epics.caget("LPQ:ccd2:TIFF1:FilePath",as_string=True))
        epics.caput(f"LPQ:ccd2:TIFF{procstr}:FileNumber", nextid)
        fname = "{}_BG_{}".format(epics.caget("LPQ:ccd2:TIFF1:FileName",as_string=True),fext)
        epics.caput(f"LPQ:ccd2:TIFF{procstr}:FileName", fname)
        epics.caput(f"LPQ:ccd2:TIFF{procstr}:WriteFile", 1)
        epics.caput("LPQ:ccd2:TIFF1:AutoSave", asavestate)
        #epics.caput("LPQ:ccd2:cam1:ImageMode", 0)
        statuspv.put("BG acquisition done")
        waitccdidle("LPQ:ccd2:cam1:DetectorState_RBV", 30)
        epics.caput("LPQ:ccd2:cam1:GreatEyesSync", 1) #enabling sync out
        ## End of ccdbg

    ## epics channels
    statuspv = epics.PV("LPQ:ccd2:progress_text1", auto_monitor=False)
    progresspv = epics.PV("LPQ:ccd2:progress_value", auto_monitor=False)
    srcStartpv = epics.PV("LPQ:source:proc_start", auto_monitor=False)
    srcStoppv = epics.PV("LPQ:source:proc_stop", auto_monitor=False)
    ccdAcqpv = epics.PV("LPQ:ccd2:cam1:Acquire", auto_monitor=False)
    ccdStatepv = epics.PV("LPQ:ccd2:cam1:DetectorState_RBV", auto_monitor=True)
    cancelpv = epics.PV("LPQ:pysup:aux:B01", auto_monitor=True)
    trigBurstpv = epics.PV("LPQ:laser:probe:TriggerBurst.PROC", auto_monitor=False)
    time.sleep(2)
    chidlist = []
    chidlist.append(statuspv.chid)
    chidlist.append(progresspv.chid)
    chidlist.append(srcStartpv.chid)
    chidlist.append(srcStoppv.chid)
    chidlist.append(ccdAcqpv.chid)
    chidlist.append(ccdStatepv.chid)
    chidlist.append(cancelpv.chid)
    chidlist.append(trigBurstpv.chid)

    ## epics pv names
    bandsafe = "LPQ:TRBAND:safe"
    nextbut = "LPQ:TRBAND:next"

    ProbeFdevPV = "LPQ:laser:probe:PRR_RBV"
    burst_mode = "LPQ:laser:probe:BurstMode_RBV"

    ## input values
    bgDiscard = int(epics.caget("LPQ:pysup:aux:A05"))
    bgBefore = int(epics.caget("LPQ:pysup:aux:A06"))
    nImages = int(epics.caget("LPQ:pysup:aux:A07"))
    nShots = int(epics.caget("LPQ:laser:probe:BurstLength_RBV"))
    bgAfter = int(epics.caget("LPQ:pysup:aux:A08"))

    ## Start process
    progresspv.put(0)

    ## calculate timings
    PVburstMode = epics.PV(burst_mode)
    burst_mode_val = PVburstMode.get()
    #calculate timings
    N = nShots
    ProbeFdev = epics.caget(ProbeFdevPV)
    proc_time = (N/(100/ProbeFdev))
    
    if burst_mode_val == 1:
        ccdtime = proc_time+1
    else:
        ccdtime = proc_time



    # take bgDiscard+bgBefore BG images and save last bgBefore average image
    if checkcancel(cancelpv):
        cleanexit(chidlist)
        return
    if bgBefore>0:
        ccdbg(ccdtime=ccdtime, bgDiscard=bgDiscard, bgN=bgBefore, proc=2, statuspv=statuspv, cancelpv=cancelpv)
        statuspv.put("BG Before : OK")
    if checkcancel(cancelpv):
        cleanexit(chidlist)
        return

    ## setup
    #set CCD exposure time
    epics.caput("LPQ:ccd2:cam1:AcquireTime", ccdtime)
    #set CCD for single mode
    epics.caput("LPQ:ccd2:cam1:ImageMode", 0)
    autosavestate = int(epics.caget("LPQ:ccd2:TIFF1:AutoSave"))
    epics.caput("LPQ:ccd2:TIFF1:AutoSave", 1)

    # reset profiles
    epics.caput("LPQ:ccd2:x:3:resetX", 1)
    epics.caput("LPQ:ccd2:x:5:resetX", 1)
    epics.caput("LPQ:ccd2:ABS:reset", 1)

    time.sleep(0.5)

    progresspv.put(0.0)
    ## loop for nImages
    statuspv.put("Acquiring {}/{}, {:.1f}%".format(0,nImages,0.0))

    for i in range(nImages):
        if checkcancel(cancelpv):
            return
        ## check for available target
        notsafe = int(epics.caget(bandsafe))
        if notsafe:
            statuspv.put("Moving TR...")
            t0=time.time()
            epics.caput(nextbut, 1)
            time.sleep(0.5)
            isbusy = True
            while isbusy:
                isbusy=int(epics.caget(nextbut))
                dt = time.time()-t0
                if dt>30.0:
                    statuspv.put("Timeout 30 sec. aborting.")
                    cleanexit(chidlist)
                    return
                time.sleep(1)
            statuspv.put("Continue.")
        srcStartpv.put(1)

        if burst_mode_val == 1:
            ccdAcqpv.put(1)
            time.sleep(0.5)
            trigBurstpv.put(1)
        else:
            ccdAcqpv.put(1)

        time.sleep(ccdtime+0.2)
        srcStoppv.put(1)

        #time.sleep(0.5)
        err =  ccdwait(ccdStatepv, cancelpv)
        if err:
            cleanexit(chidlist)
            return
        prog = (100.0*(i+1)/nImages)
        statuspv.put("Acquiring {}/{}, {:.1f}%".format(i+1,nImages,prog))
        progresspv.put(prog)

    # take bgDiscard+bgAfter BG images and save last bgAfter average image
    if checkcancel(cancelpv):
        cleanexit(chidlist)
        return
    if bgAfter>0:
        ccdbg(ccdtime=ccdtime, bgDiscard=bgDiscard, bgN=bgAfter, proc=3, statuspv=statuspv, cancelpv=cancelpv)
        statuspv.put("BG After : OK")

    # process BG and final spectra ?
    # save all (list?)

    waitccdidle("LPQ:ccd2:cam1:DetectorState_RBV", 30)
    epics.caput("LPQ:ccd2:TIFF1:AutoSave", autosavestate)

    statuspv.put("Acquisition completed")

    ## clear all epics channels created within this script
    cleanexit(chidlist)
    #epics.caput("LPQ:ccd2:progress_text1", "Acquisition completed")
    ## EOF
