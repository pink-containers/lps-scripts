#!/usr/bin/python3
from wlib.fwriterB import run
import time

## Entrypoint for experiment routine for LPQ branch B
print(f"[{time.asctime()}] fwriterB started")
run()
print(f"[{time.asctime()}] fwriterB ended")
