from epics import caget, caput, PV
import time
import numpy

#Button for Pump -Probe DoMagic branch A
def run(**kwargs):
    DEBUG=True
    log = kwargs["log"]
    ## code
    process_start = "LPQ:source:proc_start"
    process_stop = "LPQ:source:proc_stop"
    nshots = "LPQ:laser:pump:BurstLength_RBV"
    ccd_exposure = "LPQ:ccd1:cam1:AcquireTime"
    ccd_start = "LPQ:ccd1:cam1:Acquire"
    trig_burst = "LPQ:laser:pump:TriggerBurst.PROC"
    burst_mode = "LPQ:laser:pump:BurstMode_RBV"
    ccdmode = "LPQ:ccd1:cam1:ImageMode"
    bandsafe = "LPQ:TRBAND:safe"
    nextbut = "LPQ:TRBAND:next"
    ccd01_exposure_RBV = "LPQ:ccd1:cam1:AcquireTime_RBV"
    ProbeFdevPV = "LPQ:laser:pump:PRR_RBV"
    #ccd02_exposure_RBV = "LPQ:ccd2:cam1:AcquireTime_RBV"
    log("Button 011: start...")
    #t0=time.time()

    PVburstMode = PV(burst_mode)
    burst_mode_val=PVburstMode.get()
    if DEBUG: print("pump Burst Mode state:" + str(burst_mode_val))

    #calculate timings
    N = caget(nshots)
    ProbeFdev = caget(ProbeFdevPV)
    if DEBUG: print("Pump Freq dev is " + str(ProbeFdev))

    proc_time = (N/(100/ProbeFdev))
    if DEBUG: print("proctime:" + str(proc_time))

    ccdtime = proc_time
    caput(ccd_exposure, ccdtime)
    
    caput(ccdmode, 0)

    notsafe = int(caget(bandsafe))

    if notsafe:
        t0=time.time()
        caput(nextbut, 1)
        time.sleep(0.5)
        isbusy = True
        log("[B11] waiting for next button to be done")
        while isbusy:
            isbusy=int(caget(nextbut))
            dt = time.time()-t0
            if dt>30.0:
                log("[B11] waited more than 30 sec. aborting.")
                return
            time.sleep(1)
        log("[B11] is safe")


    #ccd01_exposure_RBV_val=caget(ccd01_exposure_RBV)

    caput(process_start, 1)

    if DEBUG: print("target started")

    caput(ccd_start, 1)

    if DEBUG: print("CCD started")
    
    time.sleep(ccdtime+0.2)
    caput(process_stop, 1)

    #t1=time.time()
    #log("Button 02: end. dt: {:.3f}s".format(t1-t0))
    log("Button 11: end")
    ## EOF


