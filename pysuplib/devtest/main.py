import devices
import time

def main():
    ## create devices
    dev1 = devices.DEV1()
    dev2 = devices.DEV2()
    dev3 = devices.DEV3()
    tsdev = devices.TSDEV()
    devlist = []
    devlist.append(dev1)
    devlist.append(dev2)
    devlist.append(dev3)
    devlist.append(tsdev)

    ## connect epics PVs
    devices.connect(devlist)
    print("Connecting devices epics channels...")
    time.sleep(2)
    print("Running...")
    
    ## create writer
    w = devices.Writer()
    w.set_datapath("/home/nil/LPQ/lps-scripts/pysuplib/devtest/data")
    w.newfile("mydevtest")   

    ## take devices snapshot
    devices.snapshot(devlist, w)
      
    ## create devices datasets
    devices.createdatasets(devlist, w)

    for i in range(5):
        resp = dev3.waitnewbeta(timeout=6)
        if resp == False:
            print("device 3 timeout! aborting.")
            break
        print(f"i: {i}")
        devices.appenddata(devlist, w)

    w.close()


main()
print("OK")