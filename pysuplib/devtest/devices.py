from devlib.writer import Writer
from devlib.dev1 import DEV1
from devlib.dev2 import DEV2
from devlib.dev3 import DEV3
from devlib.timestamp import TSDEV

def connect(devlist):
    for d in devlist:
        d.connect()

def createdatasets(devlist, w):
    for d in devlist:
        d.createdatasets(w=w)

def appenddata(devlist, w):
    for d in devlist:
        d.appenddata(w=w)

def snapshot(devlist, w):
    for d in devlist:
        d.snapshot(w=w)