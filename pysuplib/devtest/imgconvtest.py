import numpy as np
import time

#a = (np.random.rand(3,3)*65534)
#b = np.array(a, dtype=np.uint16)

xdim = 2052
ydim = 2048
data = []

t0=time.time()
for i in range(3):
    data.append(np.random.rand(ydim,xdim)*65534)
t1=time.time()
print("dt: {:.3f} sec".format(t1-t0))

data2 = []
t0=time.time()
for a in data:
    data2.append(np.array(a, dtype=np.uint16))
t1=time.time()


print("dt: {:.3f} sec".format(t1-t0))
print(np.shape(data2))
print(data2[0])

print("ok")