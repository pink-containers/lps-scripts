from epics import caget, caput
import time
import numpy

def run(**kwargs):
    log = kwargs["log"]
    ## code
    log("Button 01")
    caput("LPQ:sma1:m1.VAL", 1000)
    time.sleep(3)
    caput("LPQ:sma1:m1.VAL", 0)
    ## EOF
